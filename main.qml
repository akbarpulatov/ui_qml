import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import MqttClient 1.0

Window {
    visible: true
    width: 1260
    height: 960
    title: qsTr("UZAUTO-INZI ANDON MONITORING SYSTEM")
    id: root

    property var tempSubscription: 0
    property string ucdata: ""

    MqttClient {
        id: client
        hostname: hostnameField.text
        port: portField.text

        onMessageReceived: {
            //ucdata=msg;
            console.log("data is "+ ucdata);

        }




    }

    ListModel {
        id: messageModel
    }

    function addMessage(payload)
    {
        messageModel.insert(0, {"payload" : payload})

        ucdata=payload

        if(ucdata[0]  === '0') rect.color="white"
        if(ucdata[0]  === '1') { rect.color="lime"; labelSMs.text = "Вызов: " }
        if(ucdata[0]  === '2') rect.color="yellow"
        if(ucdata[0]  === '4') rect.color="red"

//        if(ucdata[1]  === '0') labelSMs.text = "Вызов: "
        if(ucdata[1]  === '1') labelSMs.text = "Вызов: НАЧАЛЬНИК "
        if(ucdata[1]  === '2') labelSMs.text = "Вызов: OTK"
        if(ucdata[1]  === '4') labelSMs.text = "Вызов: НАЛАДЧИК"

//        if(ucdata==="00") rect.color="white"
////         if(ucdata==="20") rect.color="yellow"
//          if(ucdata==="10") rect.color="lime"
//           if(ucdata==="40") rect.color="red"


        if (messageModel.count >= 100)
            messageModel.remove(99)
    }

    GridLayout {
        anchors.fill: parent
        anchors.margins: 10
        columns: 2

        Label {
            text: "Hostname:"
            enabled: client.state === MqttClient.Disconnected
        }

        TextField {
            id: hostnameField
            Layout.fillWidth: true
            text: "test.mosquitto.org"
            placeholderText: "<Enter host running MQTT broker>"
            enabled: client.state === MqttClient.Disconnected
        }

        Label {
            text: "Port:"
            enabled: client.state === MqttClient.Disconnected
        }

        TextField {
            id: portField
            width: 400
            //Layout.fillWidth: true
            text: "1883"
            placeholderText: "<Port>"
            inputMethodHints: Qt.ImhDigitsOnly
            enabled: client.state === MqttClient.Disconnected
        }

        Button {
            id: connectButton
            Layout.columnSpan: 2
            Layout.fillWidth: true
            text: client.state === MqttClient.Connected ? "Disconnect" : "Connect"
            onClicked: {
                if (client.state === MqttClient.Connected) {
                    client.disconnectFromHost()
                    messageModel.clear()
                    tempSubscription.destroy()
                    tempSubscription = 0
                } else
                    client.connectToHost()
            }
        }

        RowLayout {
            enabled: client.state === MqttClient.Connected
            Layout.columnSpan: 2
            Layout.fillWidth: true

            Label {
                text: "Topic:"
            }

            TextField {
                id: subField
//                placeholderText: "Islomxuja"
                text:  "Islomxuja"
                Layout.fillWidth: true
                enabled: tempSubscription === 0
            }

            Button {
                id: subButton
                text: "Subscribe"
                visible: tempSubscription === 0
                onClicked: {
                    if (subField.text.length === 0) {
                        console.log("No topic specified to subscribe to.")
                        return
                    }
                    tempSubscription = client.subscribe(subField.text)
                    tempSubscription.messageReceived.connect(addMessage)
                }
            }
        }

        ListView {
            id: messageView
            model: messageModel
            height: 300
            width: 200
            Layout.columnSpan: 2
            Layout.fillHeight: true
//            Layout.fillWidth: true
            clip: true
            delegate: Rectangle {
                width: messageView.width
                height: 30
                color: index % 2 ? "#DDDDDD" : "#888888"
                radius: 5
                Text {
                    text: payload
                    anchors.centerIn: parent
                }
            }
            opacity: 0
        }

        Label {
            function stateToString(value) {
                if (value === 0)
                    return "Disconnected"
                else if (value === 1)
                    return "Connecting"
                else if (value === 2)
                    return "Connected"
                else
                    return "Unknown"
            }

            Layout.columnSpan: 2
            Layout.fillWidth: true
            color: "#333333"
            text: "Status:" + stateToString(client.state) + "(" + client.state + ")"
            enabled: client.state === MqttClient.Connected
        }
    }


    Image {
        id: image
        x: 231
        y: 243
        width: 1005
        height: 694
        source: "qrc:/output-onlinepngtools.png"
        fillMode: Image.Stretch
    }
    Rectangle {
        id: rect
        x: 282
        y: 280
        width: 30
        height: 30
        color: "white"
        radius: 15
        border.color: "black"
    }

    Label {
        id: labelSMs
        x: 421
        y: 200
        text: qsTr("Вызов:")
        font.pointSize: 23
    }
}
